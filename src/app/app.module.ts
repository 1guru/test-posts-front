import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {AppComponent} from "./app.component";
import {LoginComponent} from "./login/login.component";
import {AuthenticationService} from "./services/authentication.service";
import {UploadPostComponent} from "./posts/upload-post.component";
import {PostsService} from "./services/posts.service";
import {FileSelectDirective} from "ng2-file-upload";
import {StatsService} from "./services/stats.service";


@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        UploadPostComponent,
        FileSelectDirective
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule
    ],
    providers: [
        AuthenticationService,
        PostsService,
        StatsService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
