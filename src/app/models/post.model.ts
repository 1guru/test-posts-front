export interface IPost {
    id: number;
    user_id: number;
    title: string;
    image_path: string;
}