import {Component, OnInit} from "@angular/core";
import {Observable} from "rxjs";
import {IPost} from "./models/post.model";
import {PostsService} from "./services/posts.service";
import {StatsService} from "./services/stats.service";
import {API_URL} from "../environments/environment";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    user: any;
    stats: any;
    posts: IPost[];
    apiUrl: string = API_URL;
    uploaded: boolean = false;

    constructor(private postsService: PostsService,
                private statsService: StatsService) {
    }

    ngOnInit(): void {
        let timer = Observable.timer(0, 200);
        timer.subscribe(() => {
            this.user = JSON.parse(localStorage.getItem('currentUser'));
        });

        let timerPosts = Observable.timer(0, 15 * 1000);
        timerPosts.subscribe(() => {
            this.getStats();
        });

        this.statsService.incrementViews().subscribe();

        this.getPosts();
    }

    uploadedChange(event) {
        this.getPosts();
    }

    getPosts(): void {
        this.postsService.getPosts()
            .subscribe(posts => {
                    this.posts = posts;
                },
                error => {
                    console.log(error);
                });
    }

    getStats() {
        this.statsService.getStats()
            .subscribe(stats => {
                    this.stats = stats;
                },
                error => {
                    console.log(error);
                });
    }

    downloadExport() {
        (<any> window).location.href = '';
    }
}
