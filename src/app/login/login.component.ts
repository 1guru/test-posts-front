import {Component, OnInit} from "@angular/core";
import {AuthenticationService} from "../services/index";

@Component({
    selector: 'login',
    templateUrl: 'app/login/login.component.html'
})

export class LoginComponent {
    model: any = {};
    loading = false;
    error = '';

    constructor(private authenticationService: AuthenticationService) {
    }

    login() {
        this.loading = true;
        this.authenticationService.login(this.model.username, this.model.password)
            .subscribe(result => {
                if (result === true) {
                    this.loading = false;
                } else {
                    this.error = 'Username or password is incorrect';
                    this.loading = false;
                }
            });
    }
}
