import {Injectable} from "@angular/core";
import {Http, RequestOptions, Headers, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/do";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import {API_URL} from "../../environments/environment";

@Injectable()
export class StatsService {
    public token: string;
    protected _headerOptions: RequestOptions = new RequestOptions({headers: new Headers({'Content-Type': 'application/json'})});

    constructor(private http: Http) {
    }

    getStats(): Observable<any> {
        return this.http.get(API_URL + '/api/stats', this._headerOptions)
            .map((response: Response) => <any> response.json());
    }

    incrementViews(): Observable<any> {
        return this.http.get(API_URL + '/api/stats/increment-views', this._headerOptions);
    }
}