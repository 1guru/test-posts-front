import {Injectable} from "@angular/core";
import {Http, Response, RequestOptions, Headers} from "@angular/http";
import {Observable} from "rxjs";
import "rxjs/add/operator/map";
import {API_URL} from "../../environments/environment";

@Injectable()
export class AuthenticationService {
    public token: string;
    protected _headerOptions: RequestOptions = new RequestOptions({headers: new Headers({'Content-Type': 'application/json'})});

    constructor(private http: Http) {
        // set token if saved in local storage
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
    }

    login(username: string, password: string): Observable<boolean> {
        return this.http.post(API_URL + '/oauth/token', JSON.stringify({
            username: username,
            password: password,
            grant_type: "password",
            client_id: "3",
            client_secret: "pBc8S07HrwVW0kP485KoQJWwXBoM5QFV0gnqmBCb"
        }), this._headerOptions)
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let token = response.json() && response.json().access_token;
                if (token) {
                    // set token property
                    this.token = token;

                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify({username: username, token: token}));

                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            });
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('currentUser');
    }
}