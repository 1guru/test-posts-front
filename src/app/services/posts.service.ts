import {Injectable} from "@angular/core";
import {Http, RequestOptions, Headers, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/do";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import {IPost} from "../models/post.model";
import {API_URL} from "../../environments/environment";

@Injectable()
export class PostsService {
    public token: string;
    protected _headerOptions: RequestOptions = new RequestOptions({headers: new Headers({'Content-Type': 'application/json'})});

    constructor(private http: Http) {
        // set token if saved in local storage
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
        this._headerOptions.headers.set('Authorization', `Bearer ${this.token}`);
    }

    getPosts(): Observable<IPost[]> {
        return this.http.get(API_URL + '/api/posts', this._headerOptions)
            .map((response: Response) => <IPost[]> response.json());
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('currentUser');
    }
}