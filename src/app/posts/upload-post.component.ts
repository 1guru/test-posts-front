import {Component, Input, Output, EventEmitter} from "@angular/core";
import {AuthenticationService} from "../services/index";
import {FileUploader} from "ng2-file-upload";
import {API_URL} from "../../environments/environment";
import {IPost} from "../models/post.model";

@Component({
    selector: 'app-posts-upload',
    templateUrl: 'app/posts/upload-posts.component.html'
})

export class UploadPostComponent {
    model: any = {};
    posts: IPost[];
    @Input() uploaded = false;
    @Output() uploadedChange = new EventEmitter();

    public uploader: FileUploader = new FileUploader({
        url: API_URL + '/api/posts',
        headers: [{
            name: 'Authorization',
            value: 'Bearer ' + JSON.parse(localStorage.getItem('currentUser')).token
        }]
    });

    constructor(private authenticationService: AuthenticationService) {
    }

    upload() {
        this.uploader.onBuildItemForm = (fileItem: any, form: any) => {
            if(this.model.title == undefined) {
                this.model.title = "";
            }
            form.append('title', this.model.title);
        };
        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            this.uploaded = true;
            this.uploadedChange.emit({
                value: this.uploaded
            })
        };
        this.uploader.uploadAll();
    }

    logout(): void {
        this.authenticationService.logout();
    }

}
